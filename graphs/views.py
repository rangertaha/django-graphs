#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail, BadHeaderError
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.contrib.auth import *
from django.core.paginator import Paginator
from django.conf import settings
from django.views.decorators.csrf import csrf_protect
from django.utils import simplejson

from models import *


def index(request):
    data = {'request': request, 'page':'page',}
    data.update(csrf(request))
    return render_to_response("graphs/index.html", data)


#_____________________________________________________________________________#
def get_data(request):
    try:
        ln_id = int(request.GET.get('line'))
        limit = int(request.GET.get('limit'))
        data = eval(str(list(Data.objects.filter(
            series__id__exact=ln_id )[:limit])))
    except:
        data = []
    return HttpResponse(simplejson.dumps(data), mimetype='application/json')


#_____________________________________________________________________________#
def get_update(request):
    try:
        ln_id = int(request.GET.get('line'))
        data = eval(str(Data.objects.filter(
            series__id__exact=ln_id).latest("id")))
        #import random, datetime
        #y = random.randrange(80, 100, 1)
        #y=80
        #x = int(datetime.datetime.now().strftime("%s")) * 1000

        #data = [x, y]

    except:
        data = []
    return HttpResponse(simplejson.dumps(data), mimetype='application/json')


#_____________________________________________________________________________#
def graphs(request):
    data = {'request': request, 'page':'page',}
    data.update(csrf(request))
    return render_to_response("graphs/index.html", data)

