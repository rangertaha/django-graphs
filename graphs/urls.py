#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
from django.conf.urls.defaults import patterns, include, url


urlpatterns = patterns('',
url(r'^$', 'webapp.graphs.views.index', name='graphs'),
url(r'^test/$', 'webapp.graphs.views.graphs', name='test_graphs'),
url(r'^data/$', 'webapp.graphs.views.get_data', name='graph_data'),
url(r'^update/$', 'webapp.graphs.views.get_update', name='graph_update'),
)

