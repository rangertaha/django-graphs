{% if graph.dynamic %}
    events:{
        load:function requestAllUpdates() {
            //setup an array of AJAX options, each object is an index that will specify information for a single AJAX request
            var ajaxes = [{% for series in graph.series.all %} { url:'http://127.0.0.1:8000/graphs/update/?line={{ series.id }}', dataType:'json' }, {% endfor %}],
        current = 0;
        //declare your function to run AJAX requests
        function do_ajax() {
            //check to make sure there are more requests to make
            if (current < ajaxes.length) {
                //make the AJAX request with the given data from the `ajaxes` array of objects
                $.ajax({
                    url:ajaxes[current].url,
                    dataType:ajaxes[current].dataType,
                    success:function (serverResponse) {
                        //increment the `current` counter and recursively call this function again
                        window.chart
                        {
                            {
                                graph.id
                            }
                        }
                        .
                        series[current].addPoint([serverResponse[0], serverResponse[1]], true, true);
                        // window.chart
                        {
                            {
                                graph.id
                            }
                        }
                        .
                        series[current].addPoint(serverResponse[1]);
                        // window.chart
                        {
                            {
                                graph.id
                            }
                        }
                        .
                        series[current].addPoint(serverResponse[1], true, true);
                        current++;
                        do_ajax();

                    }
                });
            }
        }

        do_ajax();
        setTimeout(requestAllUpdates, 1000);
    },
}
{% endif %} // Event