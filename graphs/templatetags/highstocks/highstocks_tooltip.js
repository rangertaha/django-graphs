{% if graph.plotOptions %}tooltip:{
animation:true,
// backgroundColor:rgba(255 255, 255, 0.85),
// borderColor:"auto",
borderRadius:5,
borderWidth:2,
// changeDecimals:null,
crosshairs:true,
enabled:true,
// formatter:null,
// headerFormat:null,
//pointFormat:null,//<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>
// positioner:null,
shadow:true,
shared:true,
snap:10 / 25,
// style:null,
useHTML:false,
// xDateFormat:null,
// yDecimals:null,
yPrefix:"",
ySuffix:"",
},{% endif %}