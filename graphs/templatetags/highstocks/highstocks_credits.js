{% if graph.credits and graph.credits.active %}credits:{
    enabled:{{ graph.credits.enabled|default:'false' }},
    href:"{{ graph.credits.href|default:'http://www.int4ops.com' }}",
    position:{
    align:'right',
    x:-10,
    verticalAlign:'bottom',
    y:-5
    },
    style:
    {{ graph.credits.style|safe|default:"{cursor: 'pointer', color: '#909090', fontSize: '10px,'}" }},
    text:"{{ graph.credits.text|default:'www.int4ops.com' }}",
},{% endif %}