{% if graph.series.all %}series:[
{% for series in graph.series.all %}
    {% if series.active %} {
        id:'{{ series.id }}',
        name:'{{ series.name }}',
        data:{% if series.demo  %}(function () {var data = [], time = (new Date()).getTime(), i; for (i = -100; i <= 0; i++) { data.push([ time + i * 1000, Math.round(Math.random() * 100) ]); } return data; })(),
        {% else %} [],
        {% endif %}
        index:{{ series.index|default:"undefined" }},
        stack:{{ series.stack|default:"null" }},
        type:"{{ series.type|default:"line" }}",
        xAxis:{{ series.xAxis|default:"0" }},
        yAxis:{{ series.yAxis|default:"0" }},
        },
    {% endif %}
{% endfor %}
],{% else %}series:[],{% endif %}







