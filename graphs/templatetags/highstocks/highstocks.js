<script type="text/javascript">  $(function () { {% for graph in graphs.all %}
    var chart_{{ graph.id }} = $('#graph_container_{{ graph.id }}').highcharts('StockChart', {
        {% include "highstocks/highstocks_chart.js" %}
        {% include "highstocks/highstocks_title.js" %}
        {% include "highstocks/highstocks_series.js" %}
        {% include "highstocks/highstocks_colors.js" %}
        {% include "highstocks/highstocks_exporting.js" %}
        {% include "highstocks/highstocks_credits.js" %}
        {% include "highstocks/highstocks_global.js" %}
        {% include "highstocks/highstocks_labels.js" %}
        {% include "highstocks/highstocks_land.js" %}
        {% include "highstocks/highstocks_legend.js" %}
        {% include "highstocks/highstocks_loading.js" %}
        {% include "highstocks/highstocks_navigation.js" %}
        {% include "highstocks/highstocks_navigator.js" %}
        {% include "highstocks/highstocks_plotoptions.js" %}
        {% include "highstocks/highstocks_rangeselector.js" %}
        {% include "highstocks/highstocks_subtitle.js" %}
        {% include "highstocks/highstocks_tooltip.js" %}
        {% include "highstocks/highstocks_xaxis.js" %}
        {% include "highstocks/highstocks_yaxis.js" %}
    }); {% endfor %} }); </script>