{% if graph.title and graph.title.active %}title:{
                    text:"{{ graph.title.text|default:'Graph' }}",
                    align:"{{ graph.title.align|default:'center' }}",
                    floating:{{ graph.title.floating|default:'false' }},
                    margin:{{ graph.title.margin|default:'15' }},
                    style:{{ graph.title.style|safe|default:'null' }},
                    useHTML:{{ graph.title.usehtml|default:'false' }},
                    verticalAlign:"{{ graph.title.verticalalign|default:'top' }}",
                    x:{{ graph.title.x|default:'0' }},
                    y:{{ graph.title.y|default:'0' }},
                },{% endif %}

