{% if graph.rangeselector %}rangeSelector:{
// buttonSpacing: 0,
// buttonTheme:
buttons:[
{
type:'month',
count:1,
text:'1m'
},
{
type:'month',
count:3,
text:'3m'
},
{
type:'month',
count:6,
text:'6m'
},
{
type:'ytd',
text:'YTD'
},
{
type:'year',
count:1,
text:'1y'
},
{
type:'all',
text:'All'
}
],
enabled:true,
// inputBoxStyle:
// inputDateFormat: %b %e %Y,
// inputEditDateFormat: %Y-%m-%d
// inputEnabled:
inputPosition:{ align:"right" },
// inputStyle:
// labelStyle:
selected:1,
},{% endif %}