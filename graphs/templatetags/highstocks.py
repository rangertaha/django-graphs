#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
#_____________________________________________________________________________#
from django.db.models import get_model
from django.template import Library, Node, TemplateSyntaxError, Variable, \
    resolve_variable
from django.utils.translation import ugettext as _

from .. import models
from .. import settings

register = Library()

@register.inclusion_tag('highstocks/highstocks.js')
def high_stocks_js(graph_name=None):
    """
    """
    print graph_name
    if graph_name != None:
        graphs = models.HighStocksGraph.objects.filter(name=graph_name)
    else:
        graphs = models.HighStocksGraph.objects.filter(active=True)
    return {'graphs': graphs}


@register.inclusion_tag('highstocks/highstocks.html')
def high_stocks(graph_name):
    """
    """
    graph = models.HighStocksGraph.objects.get_or_create(name=graph_name)[0]
    print graph.id, graph_name
    return {'high_stocks_graph': graph}




