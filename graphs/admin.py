#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
from django.contrib import admin
from models import *


admin.site.register(Color)
admin.site.register(Credit)
admin.site.register(Graph)
admin.site.register(Chart)
admin.site.register(Series)
admin.site.register(Data)
admin.site.register(Title)
admin.site.register(Exporting)
admin.site.register(HighStocksGraph)
#admin.site.register()
#admin.site.register()
#admin.site.register()
#admin.site.register()



