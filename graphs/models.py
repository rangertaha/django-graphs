#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
"""
import datetime

from django.contrib.auth.models import User
from django.db import models
from django.forms import ModelForm, Textarea, TextInput
from django.forms.extras.widgets import *

LINE_TYPE = (
    ('line', 'Line'),
    ('spline', 'Spline'),
    ('area', 'Area'),
    ('areaspline', 'Area Spline'),
    ('column', 'Column'),
    ('scatter', 'Scatter'),
    ('ohlc', 'Ohlc'),
    ('candlestick', 'Candle Stick'),
    ('arearange', 'Area Range'),
    ('areasplinerange', 'Area Spline Range'),
    ('columnrange', 'Column Range'),)
COLOR_TYPE = (
    ('#FFFFFF', 'White'),
    ('#C0C0C0', 'Silver'),
    ('#808080', 'Grey'),
    ('#000000', 'Black'),
    ('#FFA500', 'Orange'),
    ('#A52A2A', 'Brown'),
    ('#800000', 'Maroon'),
    ('#008000', 'Green'),
    ('#808000', 'Olive'),
    ('#FF0000', 'Red'),
    ('#00FFFF', 'Cyan'),
    ('#0000FF', 'Blue'),
    ('#0000A0', 'Dark Blue'),
    ('#ADD8E6', 'Light Blue'),
    ('#800080', 'Purple'),
    ('#FFFF00', 'Yellow'),
    ('#00FF00', 'Lime'),
    ('#FF00FF', 'Fuchsia'), )
HTML_SIDES = (
    ('top', 'Top'),
    ('bottom', 'Bottom'),
    ('left', 'Left'),
    ('right', 'Right'), )
HTML_HSIDES = (
    ('left', 'Left'),
    ('center', 'Center'),
    ('right', 'Right'), )
HTML_VSIDES = (
    ('top', 'Top'),
    ('bottom', 'Bottom'),
    ('middle', 'Middle'), )
BOOL_TYPE = (
    ('true', 'True'),
    ('false', 'False'),)


#_____________________________________________________________________________#
class Color(models.Model):
    """
    """
    name = models.CharField(max_length=100, blank=True, null=True)
    code = models.CharField(max_length=8, blank=True, null=True)

    active = models.BooleanField(default=True)
    def __unicode__(self):
        return '%s  %s' % (self.code, self.name)


#_____________________________________________________________________________#
class Data(models.Model):
    """
    """
    name = models.CharField(max_length=100, blank=True, null=True)
    color = models.CharField(max_length=8, blank=True, null=True)
    # dataLabels
    # marker
    # events
    x = models.BigIntegerField(blank=True, null=True)    # X
    y = models.FloatField(blank=True, null=True)         # Y

    def __unicode__(self):
        return '%s, %s' % (self.x, self.y)

    def __repr__(self):
        return '[%s, %s]' % (int(self.x), float(self.y))

    #def __new__(cls):
        #return '[%s, %s]' % (int(self.datetime), float(self.number))



#_____________________________________________________________________________#
class Series(models.Model):
    """
    """
    name = models.CharField(max_length=200, blank=True, null=True)
    index = models.IntegerField(max_length=3, blank=True, null=True)
    stack = models.CharField(max_length=8, blank=True, null=True)
    type = models.CharField(max_length=30, choices=LINE_TYPE, blank=True,
        null=True, default="line")
    xAxis = models.IntegerField(max_length=3, blank=True, null=True)
    yAxis = models.IntegerField(max_length=3, blank=True, null=True)
    data = models.ManyToManyField(Data, blank=True, null=True)
    limit = models.CharField(max_length=8, blank=True, null=True)
    demo = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    def __unicode__(self):
        return '%s' % (self.name)

#_____________________________________________________________________________#
class Chart(models.Model):
    """
    """
    ZOOM_TYPE = (
        ('x', 'X'),
        ('y', 'Y'),
        ('xy', 'X & Y'))
    name = models.CharField(max_length=100, blank=True, null=True)
    alignticks = models.CharField(max_length=8, choices=BOOL_TYPE, blank=True,
        null=True)
    animation = models.CharField(max_length=8, choices=BOOL_TYPE, blank=True,
        null=True)
    backgroundcolor = models.CharField(max_length=8, choices=COLOR_TYPE,
        blank=True, null=True)
    bordercolor = models.CharField(max_length=8, choices=COLOR_TYPE,
        blank=True, null=True)
    borderradius = models.IntegerField(max_length=3, blank=True, null=True,
        default=5)
    borderwidth = models.IntegerField(max_length=3, blank=True, null=True,
        default=0)
    ignorehiddenseries = models.CharField(max_length=8, choices=BOOL_TYPE,
        blank=True, null=True)
    panning = models.CharField(max_length=8, choices=BOOL_TYPE, blank=True,
        null=True)
    plotbackgroundcolor = models.CharField(max_length=8, choices=COLOR_TYPE,
        blank=True, null=True)
    plotbackgroundimage = models.CharField(max_length=400, blank=True,
        null=True)
    plotbordercolor = models.CharField(max_length=8, choices=COLOR_TYPE,
        blank=True, null=True)
    plotborderwidth = models.IntegerField(max_length=3, blank=True,
        null=True)
    plotshadow = models.CharField(max_length=8, choices=BOOL_TYPE,
        blank=True, null=True)
    reflow = models.CharField(max_length=8, choices=BOOL_TYPE,
        blank=True, null=True)
    renderto = models.CharField(max_length=30, blank=True, null=True)
    selectionmarkerfill = models.CharField(max_length=40, blank=True,
        null=True, default="rgba(69114,167,0.25)")
    shadow = models.TextField(blank=True, null=True)
    marginbottom = models.IntegerField(max_length=3, blank=True, null=True)
    marginleft = models.IntegerField(max_length=3, blank=True, null=True)
    marginright = models.IntegerField(max_length=3, blank=True, null=True)
    margintop = models.IntegerField(max_length=3, blank=True, null=True)
    spacingbottom = models.IntegerField(max_length=3, blank=True, null=True)
    spacingleft = models.IntegerField(max_length=3, blank=True, null=True)
    spacingright = models.IntegerField(max_length=3, blank=True, null=True)
    spacingtop = models.IntegerField(max_length=3, blank=True, null=True)
    style = models.TextField(blank=True, null=True) # Text, Null
    classname = models.CharField(max_length=100, blank=True, null=True)

    height = models.IntegerField(max_length=3, blank=True, null=True)
    width = models.IntegerField(max_length=3, blank=True, null=True)

    type = models.CharField(max_length=30, choices=LINE_TYPE, blank=True,
        null=True)
    zoomtype = models.CharField(max_length=30, choices=ZOOM_TYPE, blank=True,
        null=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        """
        """
        return '%s %s' % (self.id, self.name)


#_____________________________________________________________________________#
class Exporting(models.Model):
    """
    """
    enabled = models.CharField(max_length=8, choices=BOOL_TYPE, blank=True,
        null=True, )
    filename = models.CharField(max_length=200, blank=True, null=True)
    type = models.CharField(max_length=200, blank=True, null=True)
    floating = models.CharField(max_length=8, choices=BOOL_TYPE, blank=True,
        null=True)
    margin  = models.IntegerField(max_length=3, blank=True, null=True)

    url = models.CharField(max_length=300, blank=True, null=True)
    width = models.IntegerField(max_length=5, blank=True, null=True)

    #buttons =

    active = models.BooleanField(default=True)
    def __unicode__(self):
        """
        """
        return '%s %s' % (self.id, self.filname)


#_____________________________________________________________________________#
class Title(models.Model):
    """
    """
    text = models.CharField(max_length=200, blank=True, null=True,
        default="dev4ops.com")
    align = models.CharField(max_length=8, choices=HTML_HSIDES, blank=True,
        null=True, default="center")
    floating = models.CharField(max_length=8, choices=BOOL_TYPE, blank=True,
        null=True, default="false")
    margin  = models.IntegerField(max_length=3, blank=True, null=True,
        default=15)
    style = models.TextField(blank=True, null=True,
        default="""{color: '#3E576F',fontSize: '16px'} """)
    usehtml = models.CharField(max_length=8, choices=BOOL_TYPE,
        blank=True, null=True)
    verticalalign  = models.CharField(max_length=8,
        choices=HTML_VSIDES, blank=True, null=True, default="top")
    x = models.IntegerField(max_length=3, blank=True, null=True, default=0)
    y = models.IntegerField(max_length=3, blank=True, null=True, default=15)

    active = models.BooleanField(default=True)
    def __unicode__(self):
        """
        """
        return '%s %s' % (self.id, self.text)


#_____________________________________________________________________________#
class Credit(models.Model):
    """
    """
    enabled = models.CharField(max_length=8, choices=BOOL_TYPE, blank=True,
        null=True, default="true")
    href = models.CharField(max_length=100, blank=True, null=True,
        default="http://www.dev4ops.com")
    position = models.TextField(blank=True, null=True,
        default="""{align: 'right',x: -10,verticalAlign: 'bottom',y: -5}""")
    style = models.CharField(max_length=100, blank=True, null=True,
        default="""{cursor: 'pointer',color: '#909090',fontSize: '10px'}""")
    text = models.CharField(max_length=100, blank=True, null=True,
        default="dev4ops.com")
    active = models.BooleanField(default=True)
    def __unicode__(self):
        return '%s %s' % (self.id, self.text)


#_____________________________________________________________________________#
class Exporting(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class Label(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class Legend(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class Loading(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class Navigation(models.Model):
    pass


#_____________________________________________________________________________#
class Navigator(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class PlotOptions(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class RangeSelector(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class ScrollBar(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class Subtitle(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class ToolTip(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class xAxis(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class yAxis(models.Model):
    """
    """
    pass


#_____________________________________________________________________________#
class Dynamic(models.Model):
    """
    """
    pass


#___________________________________________________________________________#
class HighStocksGraph(models.Model):
    """
    """
    GRAPH_GROUPS = (
        ('server_graphs', 'server_graphs'),
        ('right_sidebar_graphs', 'right_sidebar_graphs'),
        ('center_content_graphs', 'center_content_graphs'),
        ('left_sidebar_graphs', 'left_sidebar_graphs'))
    title = models.ForeignKey(Title, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    chart = models.ForeignKey(Chart, blank=True, null=True)
    colors = models.ManyToManyField(Color, blank=True, null=True)
    credits = models.ForeignKey(Credit, blank=True, null=True)
    exporting = models.ForeignKey(Exporting, blank=True, null=True)
    labels = models.ForeignKey(Label, blank=True, null=True)
    legend = models.ForeignKey(Legend, blank=True, null=True)
    loading = models.ForeignKey(Loading, blank=True, null=True)
    navigation = models.ForeignKey(Navigation, blank=True, null=True)
    navigator = models.ForeignKey(Navigator, blank=True, null=True)
    plotoptions = models.ForeignKey(PlotOptions, blank=True, null=True)
    rangeselector = models.ForeignKey(RangeSelector, blank=True, null=True)
    scrollbar = models.ForeignKey(ScrollBar, blank=True, null=True)
    subtitle = models.ForeignKey(Subtitle, blank=True, null=True)
    tooltip = models.ForeignKey(ToolTip, blank=True, null=True)
    xAxis = models.ForeignKey(xAxis, blank=True, null=True)
    yAxis = models.ForeignKey(yAxis, blank=True, null=True)
    series = models.ManyToManyField(Series, blank=True, null=True)
    dynamic = models.ForeignKey(Dynamic, blank=True, null=True)
    order = models.IntegerField(max_length=3, blank=True, null=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        """
        """
        return '%s %s' % (self.id, self.name)


#_____________________________________________________________________________#
class Graph(models.Model):
    """
    """
    high_stocks = models.ManyToManyField(HighStocksGraph, blank=True, null=True)
    #high_charts = models.ManyToManyField(HighChartsGraph, blank=True, null=True)









